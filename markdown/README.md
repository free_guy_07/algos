
# Markdown

    Markdown is a light weight markdown language. It is readable unlike html.
    This is used in many ways especially in github,gitlab,etc.. 
    They automatically render the readme file and display them.

# This is a practice file to learn markdown

    using one ash symbol is similar to h1 tag in html
    you can alter the size of header by using multiple '#' symbols.    

## this is a h2 tag

### this is a h3 tag

this is plain text

we can write italic text by entangling the text in '*' or '_'.

This is *italic text*.

we can bold text with '**' straddling our text.

This is __bold text__

clubbing the above two results in bold italic.

now lets write a list

# singers and their songs

- Jason derulo
  - savage love
  - swalla

- blackpink
  - pretty savage
  - lovesick girls
  - how you like that

- selena gomez
  1. wolves
  2. souvenir

points to remember while using markdown

2 space intendation for any list.

for ordered list use number instead of "-"

Till now we have covered lists,text.

# Hypertext

Visit [GOOGLE](https://www.google.com) 

should use which protocol to use in the hypertext link

This is a aston martin 

![Aston marting](https://specials-images.forbesimg.com/imageserve/1151008459/960x0.jpg?fit=scale0)

Similar to hyper text but you append the text and link to '!'. 

the link is the source of image.

> this is a block quote
>
>> This is a new line

start a line with '>'.

To write multiple paragraphs in block quote leave a empty line with '>' at the start

for nested block quotes use '>>'

block quotes with lists

> my favourite car companies are
>
> - Aston martin
>   - dbx
>   - vulcan
>
>> - porche
>>   - 911
>>   - taycan

There must be a blank space between lists

---

'---' results in a horizantal rule

creating code block
leave  a tab or more than 4 spaces

    freecodecamp
    markdownguide
    this is a coding block

    write code here
    whatever you write directly goes into here

    $ echo shell
the below command is used in knowing the properties of the disk used

> `$ df -h`

```c
#include <stdio.h>
int main(){
printf("%s\n","this is a fenced code block with color highlighting");
return 0;    
}
// THis is a c code
```

following is a shell

```sh
$ echo "type here to get the text on the prompt"
```

lets create a table 

|syntax     |Description    |
|--------|-------|
|Header     | title         |
|paragraph  | text          |

create a solid check box list

- [x] wake up at 6 in the morning
- [ ] eat breakfast
- [ ] sleep early in the night

even images can be hyperlinked

[![image](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

instead of name the syntax used for image is placed in the coordinates.
