#include <stdio.h>
#include <stdlib.h>

void swap(int* a,int *b){
	int dupv=*a;
	*a=*b;
	*b=dupv;
}

void printarr(int arr[],int si)
{
	printf("the array is ");
	for (int i = 0; i < si; ++i)
	{
		printf("%d ",arr[i]);
	}printf("\n");
}

int main()
{
	int n;
	scanf("%d",&n);int arr[n];
	for (int i = 0; i < n; ++i)
	{
		scanf("%d",arr+i);
	}
	int c=0,i,j;
	for (i=0;i<(n-1); ++i)	// at the end of every loop one element will come to its position
	{						
		c=0;
		for(j=0;j<(n-1-i);j++)
		{
			if(arr[j]>arr[j+1])	// this is a stable sort
			{
				swap(&arr[j],&arr[j+1]);
			}
			else{
				c++;
			}
		}
		if(c==(n-1-i))	// in every step you compare n-1-i pairs if they are in ascending order you add 1
		{				// finally they break if c is no.of pairs then the array is ascending order then all the 
			break;		// remaining loop are redundant. so break to increase efficiency.elsewise for a sorted array you have to do n^2
		}				// computations .now solved in n computations
	}
	printarr(arr,n);
	return 0;
}