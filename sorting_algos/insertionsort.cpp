#include <iostream>
#include <cstdio>
#include <algorithm>

using namespace std;

int main(int argc, char const *argv[])
{
	int n;
	cout<<"the number of elements in the array is ";
	cin>>n;
	int arr[n];
	for (int i = 0; i < n; ++i)
	{
		cin>>arr[i];
	}
	for (int i = 0;i<(n-1); ++i)	// at every loop we try to keep the smallest possible at ith position
	{
		for(int j=i+1;j<n;++j)
		{
			if(arr[i]>arr[j]){
				swap(arr[i],arr[j]);
			}
		}
	}
	cout<<"the sorted array is ";
	for (int i = 0; i < n; ++i)
	{
		cout<<arr[i]<<" ";
	}cout<<endl;
	return 0;
}

/*
this is insertion sort.
*/