#include <stdio.h>
#include <stdlib.h>

void swap(int* a,int *b){
	int dupv=*a;
	*a=*b;
	*b=dupv;
}

void printarr(int arr[],int si)
{
	printf("the array is ");
	for (int i = 0; i < si; ++i)
	{
		printf("%d ",arr[i]);
	}printf("\n");
}

int main(void)
{
	int n;
	scanf("%d",&n);
	int arr[n];
	for (int i = 0; i < n; ++i)
	{
		scanf("%d",arr+i);
	}
	int i,j,c,min_index;
	for(i=0;i<(n-1);i++)
	{
		c=0;
		min_index=i;	
		for(j=i+1;j<n;j++)
		{
			if(arr[j]<arr[min_index])
			{
				min_index=j;
				c++;
			}
		}
		if(c>0)	// if c=0 no usage of swapping
		{
			swap(&arr[i],&arr[min_index]);
		}
	}
	printarr(arr,n);
	return 0;
}