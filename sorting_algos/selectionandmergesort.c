/*
    this is a implementation of both merge and selection sort to sort the array. 
    we make selection sort for the sub arrays (after dividing) whose size is less than 4.
*/


#include <stdio.h>
#include <stdlib.h>

/*
    merge function can also be used to join two sorted arrays
*/

void swap(int* a,int* b){
    int dupv=*a;
    *a=*b;
    *b=dupv;
}


void merge(int arr[],int l,int m,int r)
{
    int n1=m-l+1,n2=r-m;
    int x[n1],y[n2];
    for (int i = 0; i < n1; ++i)
    {
        x[i]=arr[i+l];
    }
    for (int i = 0; i < n2; i++)
    {
        y[i]=arr[i+m+1];
    }
    int c1=0,c2=0;
    while(c1!=n1 && c2!=n2)
    {
        if(x[c1]<=y[c2])
        {
            arr[c1+c2+l]=x[c1];
            c1++;
        }
        else
        {
            arr[c1+c2+l]=y[c2];
            c2++;
        }
    }
    while (c1!=n1)
    {
        arr[c1+c2+l]=x[c1];
        c1++;
    }
    while (c2!=n2)
    {
        arr[c1+c2+l]=y[c2];
        c2++;
    }
}

void normal_mergesort(int arr[],int l,int r)
{
    if(l<r)
    {
        if(r-l>=4)  //size >= 5
        {
            int m=(l+r)/2;
            normal_mergesort(arr,l,m);
            normal_mergesort(arr,m+1,r);
            merge(arr,l,m,r);
        }
        else
        {
            //printf("hello ");
            int mx,i,j; 
            for (i=l;i<=(r-1);++i)
            {
                mx=i;
                for(j=i+1;j<=r;++j)
                {
                    if(arr[j]<arr[mx])
                    {
                        mx=j;
                    }
                }
                swap(&arr[i],&arr[mx]);
            }
        }
    }
}

int main(void)
{
    int n;
    scanf("%d",&n);    
    int arr[n];
    for (int i = 0; i < n; i++)
    {
        scanf("%d",&arr[i]);
    }
    normal_mergesort(arr,0,n-1);
    for (int i = 0; i < n; i++)
    {
        printf("%d ",arr[i]);
    }
    printf("\n");
    return 0;
}
