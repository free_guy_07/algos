/*
AUTHOR: A.H.V.ABHINAV

	i recently learned about quick sort. i think its time for me to write my implementation,
	I am taking last element as pivot. even if we take the middle element as pivot we can swap middle and last and at last put the last element in parition value.
	what actually matters is the is the partition element . it decides the complexity as it depends the length of the tree.
	based on the height of tree the complexity changes. if no matter what always the pivot divides the array in to similar sized halves then we can finish the
	sorting faster than merge sort .

*/

#include <bits/stdc++.h>

using namespace std;

int partition(int arr[],int l,int r)
{
	int pivot,i,j;
	pivot=arr[r];
	//i=l-1;
	i=l;
	for (j=l;j<r;++j)
	{
		if(arr[j]<pivot)
		{
			//i++;
			swap(arr[i],arr[j]);
			i++;
		}
	}
	swap(arr[i],arr[r]);
	return i;
}

void quick(int arr[],int l,int r)
{
	if(r<=l)		//no need to sort a single element array
	{
		return;
	}
	int x=partition(arr,l,r);
	quick(arr,l,x-1);
	quick(arr,x+1,r);
}


int main(void)
{
	int n;
	cin>>n;
	int arr[n];
	for (int i = 0; i < n; ++i)
	{
		cin>>arr[i];
	}
	quick(arr,0,n-1);
	for (int i = 0; i < n; ++i)
	{
		cout<<arr[i]<<" ";
	}
	cout<<endl;
	return 0;
}

/*
	NOte: the pivot is the last element.
	first both the variables will be pointing to the first element. so lets check this algo like this.keep track of i while doing the loop.lets start with 0 if arr[l]<pivot. this can 
	i is moved to 1. else that should be swapped at some point of time or there are no such elements . so i is still at l. if we find i indirectly keeps track of all elements which are
	less than 0. at every point of time what i specifies that all elements from l to i-1 are < pivot. 
*/
