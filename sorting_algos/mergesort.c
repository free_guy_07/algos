#include <stdio.h>
#include <stdlib.h>

// declaring the functions
void msort(int* arr,int l,int r);
void mergef(int* arr,int l,int m,int r);

int main()
{
	int n;
	printf("the value of n is ");
	scanf("%d",&n);
	int arr[n];
	printf("give space seperated integers for reading\n");
	
	for (int i = 0; i < n; ++i)
	{	
		scanf("%d",&arr[i]);//also arr+i can be passed as address its your wish	
	}
	printf("initial array is ");
	for (int i = 0; i < n; ++i)
	{
		printf("%d ",arr[i]);
	}
	printf("\n");
	msort(arr,0,n-1); 
	
	printf("the final array is ");
	
	for (int i = 0; i < n; ++i)
	{
		printf("%d ",arr[i]);
	}

	printf("\n");
}


void msort(int* arr,int l,int r)   // we keep l<r because if l=r m=l=r.then m+1 to r doesnt make sense.
{									//	so last step of msort will be r=l+1;so two single sized arrays msort does nothing 
	if(l<r)			 				// mergef starts action			
	{	
		int m = l+(r-l)/2;
		msort(arr,l,m);
		msort(arr,m+1,r);
		mergef(arr,l,m,r);	
	}
}


void mergef(int* arr,int l,int m,int r) 	// merge two sorted arrays one array from indices [l,m] while other from indices [m+1,r]
{

	int num1=m-l+1,num2=r-m;
	int x[num1],y[num2];                    // no.of elements in first array would be num1 and second array would be num2
	for (int i=0;i<num1; ++i)
	{
		x[i]=arr[i+l]; 			
	}
	
	for (int i = 0; i < num2; ++i)			 
	{
		y[i]=arr[i+m+1];				
	}
	
	int c1=0,c2=0,c3=0;
	while(c1!=num1 && c2!=num2)  		// i want it to be a stable sorting algo .it entirely is in your hands. if i use x[c1]<y[c2] in the first loop then it is a un0stable sorting algo
	{
		if(x[c1]<=y[c2])
		{
			arr[c3+l]=x[c1];
			c1++;
		}	
		else
		{
			arr[c3+l]=y[c2];
			c2++;
		}
		c3++;
	}
	
	while(c1!=num1)
	{
		arr[c3+l]=x[c1];
		c1++;
		c3++;
	}

	while(c2!=num2)
	{
		arr[c3+l]=y[c2];
		c2++;
		c3++;
	}
}