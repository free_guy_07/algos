#include <bits/stdc++.h>

using namespace std;

#define FOR(i,n) for(int i=0;i<n;i++)
#define FORE(i,a,b) for(int i=a;i<=b;i++)
#define ll long long 
#define ld long double
#define vi vector<int>
#define pb push_back
#define ff first
#define ss second
#define ii pair<int,int>
#define iii pair<int,ii>
#define iiii pair<iii,int>
#define pll pair<ll,ll>
#define plll pair<ll,pll>
#define vv vector
#define endl '\n'

vector<int> adj[1001];	// adjacency list

bool vis[1001];		// visited arr

void bfs(int stp){
	queue<int> q;
	q.push(stp);
	cout << "visited vertex " << stp << endl;
	vis[stp] = 1;
	int x;
	while(!q.empty()){
		x = q.front();q.pop();
		for(int j:adj[x]){
			if(!vis[j]){
				cout << "visited vertex "<<j << endl;
				vis[j]=1;
				q.push(j);
			}
		}
	}
	cout << "Completed traversal of graph" << endl;
}


int main(){
	int m,n;cin>>m>>n;	// m edges n vertices 
	int x,y;
	FOR(i,m){
		cin>>x>>y;
		adj[x].pb(y);
		adj[y].pb(x);
	}
	bfs(0);
	return 0;
}