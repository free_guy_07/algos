/*
Author : A.H.V.ABHINAV

	THIS CODE IS WRITTEN USING C++ FOR CALCULATING THE NO.OF DIGITS WITH SUM S
	AND LESS THAN X. THE MAIN CONCEPT USED IS DIGIT DP.
	Note: g++ can compile both c and c++ but gcc can compile only c.

*/

#include <bits/stdc++.h>     
using namespace std;

#define FOR(i,n) for(int i=0;i<n;i++)
#define FORE(i,a,b) for(int i=a;i<=b;i++)
#define ll long long 
#define ld long double
#define vi vector<int>
#define vll vector<long long>
#define pb push_back
#define ff first
#define ss second
#define ii pair<int,int>
#define iii pair<int,ii>
#define iiii pair<iii,int>
#define pll pair<ll,ll>
#define plll pair<ll,pll>
#define vv vector
#define endl '\n'
 


ll ddp(vll v,ll n,ll sum,ll check)
{
	if(n==0 && sum==0){
		return 1;
	}
	if(n==0 || sum<0){
		return 0;
	}
	ll is=0,lpv=10;
	if(check==1){	
		is+=ddp(v,n-1,sum-v[n-1],1);
		lpv=v[n-1];
	}
	for (ll i = 0; i<lpv; ++i)
	{
		is+=ddp(v,n-1,sum-i,0);
	}
	return is;
}


int main()
{
    ios_base::sync_with_stdio(false);cin.tie(NULL);cout.tie(NULL);
    ll x,sum,n; 	// x is the number to be scanned . n is no.of digits in x. sum is required sum
    cin>>x>>sum; 	// given x,sum give the no.of integers satistying the given condition
    ll y=x;
    vll v; 
    while(y>0)			// this finally stores all the digits of the number in array where the ones 
    {					// place will be in the v[0]i.e., first element of the array
    	v.pb(y%10);		//
    	y=y/10;
    }

    n=v.size();
    cout<<"The final answer is "<<ddp(v,n,sum,1)<<endl;
    return 0;
}



/*
	when we are moving if we keep moving at the highest digit we must no cross the next digit because it crosses the value of it.
	which we dont want. so we keep a check to know whether we can cross the boundary. let me illustrate il with a example.
	3498 is x; suppose we are checking numbers which start with 2 wecan go till 9. else wise we can exploit the whole single digit range.
	so it is imperative to keep a flag to keep the place where we are in. if we dont use check it has the liberty to reach the number > x
	which is not valid according to our question. so we keep it so that it doesnt always have the privilage to go till 9 for the 
	next digit.
	suppose you want b/w [a,b] then you can do dp(b)-dp(a-1)
*/