#include <bits/stdc++.h>     
using namespace std;

#define FOR(i,n) for(int i=0;i<n;i++)
#define FORE(i,a,b) for(int i=a;i<=b;i++)
#define ll long long 
#define ld long double
#define vi vector<int>
#define vll vector<long long>
#define pb push_back
#define ff first
#define ss second
#define ii pair<int,int>
#define iii pair<int,ii>
#define iiii pair<iii,int>
#define pll pair<ll,ll>
#define plll pair<ll,pll>
#define vv vector
#define endl '\n'
 
int dp[1005][1005];

int knapsack(int v[],int w[],int n,int maxw)	// maxw is the weight of the container
{
	if(n<0){
		return 0;
	}
	if(dp[n][maxw]!=-1)
	{
		return dp[n][maxw];
	}
	if(n==0 || maxw==0)
	{
		dp[n][maxw]=0;
		return 0;
	}
	if(maxw<w[n-1])
	{
		dp[n][maxw]=knapsack(v,w,n-1,maxw);
		return dp[n][maxw];
	}
	else{
		dp[n][maxw]=max(v[n-1]+knapsack(v,w,n-1,maxw-w[n-1]),knapsack(v,w,n-1,maxw));
		return dp[n][maxw];
	}
}


 int main(void){
 	int x,n,w;
 	cin>>n>>w;
 	int val[n],wei[n];
 	memset(dp,-1,sizeof(dp));
 	FOR(i,n)
 	{
 		cin>>val[i];
 	}
 	FOR(i,n)
 	{
 		cin>>wei[i];
 	}
 	int ans=knapsack(val,wei,n,w);
 	cout<<ans<<endl;
}