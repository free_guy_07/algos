# 0/1 Knapsack Problem
## example of a problem
    Let's assume, There are n stones with a particular weight and price. There is a bag which can withhold a maximum weight of W per se. Now our aim is to fill the bag with these stones so that it is most valuble in the sense the sum of price of stones must be max.
    we cant check all possiblities which are pow(2,n). so we are solving this using dynamic programming. Either we are selecting it or omitting a particular stone.
we are going to use dynamic programming here. The logic goes like this if the wait of rock is more than remaining weight it can hold then neglect it or take the max you get when you remove one of those and add one of those 

    let R(n,q) be the max price of the bag which can be filled with first n elements.        
    then we can say 
    1. R(n,q) is R(n-1,q) if nth element weight is more than the q then it will be equal to R()
    2. R(n,q) will be max of values obtained by picking the last stone and leaving the last stone     
    R(n,q)=max(R(n-1,q-w)+v,R(n-1,q)) v,w are price and value of stone respectively
    
Finally
    
     main points while implementing logic    
      1.   dont mixup weight and price array
      2.   use memoisation
      

you can practice [here](https://practice.geeksforgeeks.org/problems/0-1-knapsack-problem0945/1)
