/*
	finding gcd of two numbers in less than logn time complexity

*/
#include <bits/stdc++.h>

using namespace std;

#define FOR(i,n) for(int i=0;i<n;i++)
#define FORE(i,a,b) for(int i=a;i<=b;i++)
#define ll long long 
#define ld long double
#define vi vector<int>
#define vs vector<string> 
#define vll vector<long long>
#define pb push_back
#define ff first
#define ss second
#define pii pair<int,int>
#define piii pair<int,ii>
#define piiii pair<iii,int>
#define pll pair<ll,ll>
#define plll pair<ll,pll>
#define vv vector
#define endl '\n'
const ll rtx=1e10;
/*
	using recursion
*/
ll gcd1(ll a,ll b);

ll gcd(ll a,ll b)
{
	if(a<b)
	{
		swap(a,b);
	}
	if(b==0)
	{
		return a;
	}
	return gcd(b,a%b);	
}


int main(int argc, char const *argv[])
{
	ll a,b;
	cin>>a>>b;
	cout<<"the gcd of the given two numbers is "<<gcd1(a,b)<<endl;
	return 0;
}
/*
	without recursion
*/
ll gcd1(ll a,ll b){
	ll c=1;
	if(a<b) swap(a,b);
	while(c!=0)
	{
		c=a%b;
		a=b; // a will become b
		b=c; // 
	}
	return a;
}
