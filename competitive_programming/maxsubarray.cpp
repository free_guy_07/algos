// this is called as kadanes algorithm
#include <iostream>
#include <algorithm>
#include <cstdio>

using namespace std;

#define endl "\n"

int main()
{
	int n;
	cout<<"the no.of elements in the array ";fflush(stdout);
	cin>>n;
	int arr[n];
	for (int i = 0; i < n; ++i)
	{
		cin>>arr[i];
	}
	int maxsumtillthen=0,ans=0;
	for (int i = 0; i < n; ++i)
	{
		//maxsumtillthen=max(arr[i],maxsumtillthen+arr[i]);
		maxsumtillthen+=arr[i];
		ans=max(ans,maxsumtillthen);	
		if(maxsumtillthen<0){
			maxsumtillthen=0;
		}
	}
	cout<<"this is the max continous sub array sum "<< ans <<endl;
	return 0;
}



/*
	this algo works because when you are moving you encountered a place where sum becomes negative then maxsumtillthen is 
	set to 0. so from the next element its like counting is started freshly
	either of them works beacause when sum is negative you start maxsumtill with next positive element

	if all are negative then the max subarray sum would be zero 
	becuase you arent selecting numbers
*/