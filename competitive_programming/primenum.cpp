/*
	to find all prime numbers from 1 to n
	with nlog(logn) complexity without checking whether each 
	one is prime or not 
*/


#include <bits/stdc++.h>

using namespace std;

#define FOR(i,n) for(int i=0;i<n;i++)
#define FORE(i,a,b) for(int i=a;i<=b;i++)
#define ll long long 
#define ld long double
#define vi vector<int>
#define vs vector<string> 
#define vll vector<long long>
#define pb push_back
#define ff first
#define ss second
#define pii pair<int,int>
#define piii pair<int,ii>
#define piiii pair<iii,int>
#define pll pair<ll,ll>
#define plll pair<ll,pll>
#define vv vector
#define endl '\n'
const ll rtx=1e10;

int chk[1000005];	// by using the track of this array finallly get which is prime

// for n is less than 10^6

int main(int argc, char const *argv[])
{
	int n;
	cin>>n;
	memset(chk,1,sizeof(chk));
	chk[0]=0;chk[1]=0;	// both are not prime
	for (int i=2; i<=n/2; ++i)
	{
		if(chk[i])
		{
			for(int j=2;i*j<=n;j++)
			{
				chk[i*j]=0;
			}
		}
	}
	FOR(i,n+1){
		cout<<i <<" is ";
		chk[i]?cout<<"prime":cout<<"non prime";
		cout<<endl;
	}
	return 0;
}