/*
	for modular exponentiation
	calculating x^y%n

*/
#include <iostream>

using namespace std;

#define ll long long

int main(int argc, char const *argv[])
{	
	ll x,y,n;
	cin>>x>>y>>n;
	ll ans=1;
	while(y>0)
	{
		if(y%2){
			ans=(ans*x)%n;
		}
		x=(x*x)%n;
	}
	cout<< "The answer is "<<ans<<endl;
	return 0;
}
