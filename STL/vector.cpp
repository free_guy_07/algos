#include <iostream>
#include <vector>
#include <cstdio>
#include <algorithm>

using namespace std;
#define ll long long
#define endl "\n"

int main()
{
	ll c=1;
	cout<<"just for fun "<<c<<endl;
	int n,x;
	cin>>n;
	vector<int> v(n,2);	// initially created a vector of size 5 with all elements equal to 2 now adding 5 elements.
	for(auto qq:v)
	{
		cout<<qq<<" ";
	}cout<<endl;
	for(int i=0;i<n;i++){
		cin>>x;
		v.push_back(x);
	}
	cout<<"now the current size of vector is "<<v.size()<<endl;
	v.pop_back();
	cout<<"now the current size of vector is "<<v.size()<<endl;
	// to sort all the elements in vector
	sort(v.begin(),v.end());
	for (std::vector<int>::iterator i = v.begin(); i != v.end(); ++i)
	{
		cout<<*i<<" ";
	}cout<<endl;
	reverse(v.begin(),v.end());
	for(auto qq:v)
	{
		cout<<qq<<" ";
	}
	cout<<endl;
	cout<<*v.begin()<<"is the starting element and the ending element is "<<v.back();
	cout<<"is the vector empty "<<v.empty()<<endl;
	v.clear(); //	to clear the vector after clear size is zero
	cout<<"is the vector empty "<<v.empty()<<endl;
	return 0;
}

// v.end() points to the theoritically last element which do not exist