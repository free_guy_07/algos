#include <iostream>
#include <set>
#include <algorithm>

using namespace std;

inline void printset(set<int> s){
	for (auto it : s)
	{
		cout<< it << " ";
	}
	cout<<endl;	
}

int main(){
	set<int> s;
	s.insert(1);
	s.insert(9);
	s.insert(2);
	s.insert(7);
	s.insert(38);
	s.insert(11);
	// printing elements of set using iterator 
	for(set<int>::iterator it=s.begin();it!=s.end();++it)
	{
		cout<< *it <<" ";
	}
	cout<<endl;
	s.erase(999);	// as 999 is not in the set there is no change
	cout<<s.count(2)<<"is count of 2\n";
	printset(s);	 
	s.erase(2);
	cout<<"2 is removed \nnow the set is ";
	printset(s);
	s.insert(33);
	s.insert(232);
	s.insert(2839);
	s.insert(34);
	s.insert(2323);
	cout<<"the size is "<<s.size()<<endl;
	set<int>::iterator iter=s.begin();
	iter++;iter++;

	cout<<"erasing first 2 elements\n";
	s.erase(s.begin(),s.find(7));
	cout<<"the size is "<<s.size()<<endl;
	printset(s);
	cout<<"is the set empty we can know by .empty() "<<s.empty()<<endl;
	cout<<"clearing all the elements of the set"<<endl;
	s.clear();
	cout<<"the set is empty? "<<s.empty()<<endl;
	return 0;
}