/*
Author: A.H.V.ABHINAV
  searching is one of the most important jobs it is imperative that we must finish
as fast as possible.  when the list is not sorted we have to go through every element but in binary search
we can decide whether the required element lies in first half or second half. 

Note : Binary search only works only when you have random access to element of the list.
binary search is useless for linked list

*/

#include <bits/stdc++.h>

using namespace std;

int main()
{
	int n,x;	// n = no of elements in array , x = element to be searched;
	cout<<"The no.of elements in the array are ";
	cin>>n;
	int arr[n];
	cout<<"now give the elements of the array\n"<<endl;
	for (int i = 0; i < n; ++i)
	{
		cin>>arr[i];
	}
	cout<<"lets search the number ";
	cin>>x;     			// x value to be searched
	int l,r,m,index=-1;
	l=0;r=n-1;                      

	while(l<=r)
	{
		m=(l+r+1)/2;
		if(arr[m]==x)
		{
			index=m;
		}
		else if(arr[m]>x)	// we are searching for 
		{
			r=m-1;
		}
		else
		{
			l=m+1;
		}
	}

	if(index==-1){
		cout<<"there is no element "<<x<<" in the array"<<endl;
	}
	else
	{
		printf("the element %d is at index %d \n",x,index);
	}
	return 0;

}


/*
	Note: I am giving index not position
	There is a small difference between index and position.in general terms position=index+1;
	there are so many applications of binary search.you can extend it to many cases
*/