#include <bits/stdc++.h>

using namespace std;

int main(){
	int m,n;	// n is the no.of elements m is number to be searched
	cout<<"the value of n is ";
	cin>>n;
	vector<int> v(n);
	cout<<"array input please ";
	for (int x,i = 0; i < n; ++i)
	{
		cin>>x;
		v.push_back(x);
	}
	cout<<"number to be searched";
	cin>>m;
	for (int i = 0; i < n; ++i)
	{
		if(v[i]==m)
		{
			cout<<"the number is at position "<<i+1<<" in the array"<<endl;
		}
	}
	v.pop_back(); // to clear the last element
	cout<<v.empty()<<endl;// returns true or false
	v.clear(); /// cleans the vector
	return 0;
}


/* 
		v.size() // returns size
		usage of vector functions
		the basis of linear search is unsorted arrays there is no point 
		in applying binary search to a unsorted arrays
*/